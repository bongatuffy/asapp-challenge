# Kyle Lewis' ASAPP Coding Challenge

This is a simple chat app that allows two users to communicate with each other (albeit on the same screen :)

The project was created with React and Redux. Here is a basic breakdown of the `src` folder structure:

- Components: reusable components
- Features: full "pages" including state connected containers
- Reducers: redux reducers
- Store: initializing the redux store

To see the shape of the redux state, see the `reducers/message-reducer'.
