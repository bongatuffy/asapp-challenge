import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Root from './root';

class App extends Component {
  render() {
    return (
      <Provider store={this.props.store}>
        <Root />
      </Provider>
    );
  }
}

export default App;
