import React from 'react';
import Form from '../form';
import Messages from '../messages';
import IsTyping from '../is-typing';

const namespace = 'chat-screen';

export default class ChatScreen extends React.Component {
  render() {
    return (
      <div className={namespace}>
        <div className='chat-screen-content'>
          <Messages
            messages={this.props.messages}
            user={this.props.user}
          />
          <IsTyping
            user={this.props.user}
            user1IsTyping={this.props.user1IsTyping}
            user2IsTyping={this.props.user2IsTyping}
          />
        </div>
        <Form
          onChangeTextInput={this.props.onChangeTextInput}
          onSubmit={this.props.onSubmit}
          formValue={this.props.formValue}
          user={this.props.user}
        />
      </div>
    )
  }
}
