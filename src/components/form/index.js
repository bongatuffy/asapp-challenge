import React from 'react';

export default class Form extends React.Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);

    this.state = { formValue: '' }
  }

  onSubmit = (e) => {
    e.preventDefault();
    const user = this.props.user;
    const content = this.state.formValue;
    this.setState({ formValue: '' })
    this.props.onSubmit({ user, content })
  }

  onChange = (e) => {
    const value = e.currentTarget.value;
    const user = this.props.user;

    this.props.onChangeTextInput({ user, value });
    this.setState({ formValue: e.currentTarget.value })
  }

  render() {
    return (
      <form
        onSubmit={this.onSubmit}
      >
        <input
          className='form-input'
          type='text'
          value={this.state.formValue}
          onChange={this.onChange}
        />
        <input
          type='submit'
          value='Send message'
        />
      </form>
    )
  }
}
