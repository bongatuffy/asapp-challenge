import React from 'react';

export default class IsTyping extends React.Component {
  constructor(props) {
    super(props);
    this.isUserTyping = this.isUserTyping.bind(this);
    this.renderContent = this.renderContent.bind(this);
  }

  isUserTyping = () => {
    if (this.props.user === 'user1' && this.props.user2IsTyping) {
      return true;
    }

    if (this.props.user === 'user2' && this.props.user1IsTyping) {
      console.log('we hit this');
      return true;
    }

    return null;
  }

  renderContent = () => {
    if (this.isUserTyping()) {
      return <div>They are typing...</div>
    }

    return null;
  }

  render() {
    return (
      <div>
        {this.renderContent()}
      </div>
    )
  }
}
