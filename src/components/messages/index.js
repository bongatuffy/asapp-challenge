import React from 'react';

export default class Messages extends React.Component {
  render() {
    const me = this.props.user;
    const them = this.props.user === 'user1' ? 'user2' : 'user1';

    const messages = this.props.messages.map((message) => {
      const messageClass = message.get('user') === me ? 'myMessage' : 'theirMessage';
      return <div className={`${messageClass} message`}>{message.get('content')}</div>;
    });

    return (
      <div>
        {messages}
      </div>
    )
  }
}
