import React from 'react';
import ChatScreen from '../../../components/chat-screen';

const namespace = 'chat-app';

export default class ChatApp extends React.Component {
  render() {
    return (
      <div className='chat-app-container'>
        <div className={namespace}>
          <ChatScreen
            onSubmit={this.props.sendMessage}
            messages={this.props.messages}
            user='user1'
            formValue={this.props.user1FormValue}
            onChangeTextInput={this.props.onChangeTextInput}
            user1IsTyping={this.props.user1IsTyping}
            user2IsTyping={this.props.user2IsTyping}
          />
          <ChatScreen
            onSubmit={this.props.sendMessage}
            messages={this.props.messages}
            user='user2'
            formValue={this.props.user2FormValue}
            onChangeTextInput={this.props.onChangeTextInput}
            user1IsTyping={this.props.user1IsTyping}
            user2IsTyping={this.props.user2IsTyping}
          />
        </div>
      </div>
    )
  }
}
