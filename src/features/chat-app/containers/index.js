import { connect } from 'react-redux';
import { sendMessage, onChangeTextInput } from '../../../reducers/message-reducer';

import ChatApp from '../components';

const mapStateToProps = (state) => {
  return {
    messages: state.messages.get('messages'),
    user1FormValue: state.messages.get('user1FormValue'),
    user2FormValue: state.messages.get('user2FormValue'),
    user1IsTyping: state.messages.get('user1'),
    user2IsTyping: state.messages.get('user2')
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onChangeTextInput: (payload) => dispatch(onChangeTextInput(payload)),
    sendMessage: (payload) => dispatch(sendMessage(payload))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatApp);
