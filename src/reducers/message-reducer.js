import Immutable from 'immutable';

const ON_CHANGE_TEXT_INPUT = 'ON_CHANGE_TEXT_INPUT';
const SEND_MESSAGE = 'SEND_MESSAGE';

export const sendMessage = (payload) => {
  return {
    type: SEND_MESSAGE,
    payload
  }
}

export const onChangeTextInput = ({ user, value }) => {
  return {
    type: ON_CHANGE_TEXT_INPUT,
    payload: { user, value }
  }
}

const defaultState = Immutable.fromJS({
  messages: Immutable.List(),
  user2FormValue: '',
  user1FormValue: '',
  user1: false,
  user2: false
});

const messageReducer = (state = defaultState, action) => {
  let user;

  switch(action.type) {
    case SEND_MESSAGE:
      user = action.payload.user
      const newMessage = Immutable.fromJS({
        content: action.payload.content,
        user: action.payload.user
      })
      const messages = state.get('messages').push(newMessage);
      return state.merge({
        messages: messages,
        [user]: false
      });
    case ON_CHANGE_TEXT_INPUT:
      user = action.payload.user;
      const value = action.payload.value;
      const isTyping = value === '' ? false : true;
      return state.set(user, isTyping);
    default:
      return state;
  };
};

export default messageReducer;
