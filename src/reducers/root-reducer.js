import {combineReducers} from 'redux';
import MessageReducer from './message-reducer';

const RootReducer = combineReducers({
  messages: MessageReducer
});

export default RootReducer;
