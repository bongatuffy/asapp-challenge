import reducer from '../message-reducer';
import { SEND_MESSAGE, ON_CHANGE_TEXT_INPUT } from '../message-reducer';

import Immutable from 'immutable';


describe('The message reducer', () => {
  beforeEach(() => {
    testContext = {};
  });

  describe('actions', () => {
    describe('SEND_MESSAGE', () => {

      beforeEach(() => {
        testContext.defaultState = Immutable.fromJS({
          messages: []
        })
      });

      it('adds a message to the messages slice of state', () => {
        const sendMessageAction = {
          type: actions.SEND_MESSAGE,
          payload: { }
        }
        expect(reducer(testContext.defaultState, sendMessage))
      });
    });
  });

});
