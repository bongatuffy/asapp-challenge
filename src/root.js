import React from 'react';
import ChatApp from './features/chat-app/containers';

export default class Root extends React.Component {
  render() {
    return (
      <div>
        <ChatApp />
      </div>
    )
  }
}
